<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function submit(request $request){
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view ('halaman.penutup',['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}
