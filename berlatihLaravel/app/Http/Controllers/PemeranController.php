<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PemeranController extends Controller
{
    public function create(){
        return view ('pemeran.tambah');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        DB::table('pemeran')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);
        return redirect('/pemeran');
    }
    public function index(){
        $pemeran = DB::table('pemeran')->get();
        return view('pemeran.tampil', ['pemeran' => $pemeran]);
    }

    public function show($id){
        $pemeran = DB::table('pemeran')->where('id', $id)->first();
 
        return view('pemeran.detail', ['pemeran' => $pemeran]);
    }

    public function edit($id){
        $pemeran = DB::table('pemeran')->where('id', $id)->first();

        return view('pemeran.edit', ['pemeran' => $pemeran]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $pemeran = DB::table('pemeran')
              ->where('id', $id)
              ->update(['nama' => $request->nama,
              'umur' => $request->umur,
              'bio' => $request->bio]);
        
              return redirect('/pemeran');
    }
    
    public function destroy($id)
    {
    $pemeran = DB::table('pemeran')->where('id', $id)->delete();
    return redirect('/pemeran');
    }

}
