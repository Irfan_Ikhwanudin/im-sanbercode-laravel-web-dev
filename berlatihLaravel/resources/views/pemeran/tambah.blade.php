@extends('layouts.master')
@section('title')
Halaman Isian
@endsection
@section ('content')
<form action="/pemeran" method="POST">
    @csrf
    <div class="mb-3">
      <label>Nama Pemeran </label>
      <input type="text" name="nama" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label>Umur Pemeran </label>
        <input type="number" id="replyNumber" min="0" step="1" data-bind="value:replyNumber" name="umur" class="form-control" >
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="mb-3">
      <label >Bio Pemeran</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    @endsection