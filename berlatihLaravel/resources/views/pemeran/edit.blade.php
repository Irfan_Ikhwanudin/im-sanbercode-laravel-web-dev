@extends('layouts.master')
@section('title')
Halaman Editing Pemeran
@endsection
@section ('content')

<div>
    <h2>Edit Pemeran{{$pemeran->id}}</h2>
    <form action="/pemeran/{{$pemeran->id}}"method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama">nama</label>
            <input type="text" class="form-control" name="nama" value="{{$pemeran->nama}}" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">umur</label>
            <input type="number" id="replyNumber" min="0" step="1" data-bind="value:replyNumber" name="umur" class="form-control" value="{{$pemeran->umur}}"  id="umur" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio"  value="{{$pemeran->bio}}"  id="bio" placeholder="Masukkan bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>






    @endsection