@extends('layouts.master')
@section('title')
Halaman List Pemeran
@endsection
@section ('content')

<a href="/pemeran/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr> 
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pemeran as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>
                        <form action="/pemeran/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/pemeran/{{$value->id}}" class="btn btn-info">Detail</a>
                            <a href="/pemeran/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                    </td>
                    
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
            </tbody>
        </table>



@endsection