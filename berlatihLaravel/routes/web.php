<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;  
use App\Http\Controllers\PemeranController; 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// entpoint/ route
Route::get('/',[HomeController::class, 'home']);
Route::get('/register',[AuthController::class, 'register']);
Route::post('/home',[ AuthController::class,'submit']);


Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});
// testing master template 
Route::get('/master', function(){
    return view('layouts.master');
});

/* CRUD pemeran */
// Create
// Form pemeran 
Route::get('/pemeran/create',[PemeranController::class,'create']);
// Kirim data ke database atau tambah data ke database
Route::post('/pemeran',[PemeranController::class,'store']);

// Read Data
// Tampil Semua data di blade

Route::get ('/pemeran', [PemeranController::class,'index']);

// Detail Pemeran Berdasarkan Id
Route::get('/pemeran/{pemeran_id}',[PemeranController::class,'show']);

// Update
// Form update
Route::get('/pemeran/{pemeran_id}/edit',[PemeranController::class,'edit']);

// Update data ke database berdasar Id
Route::put('/pemeran/{pemeran_id}',[PemeranController::class,'update']);

// Delete
// Delete Berdasarkan parameter Id
Route::delete('/pemeran/{pemeran_id}',[PemeranController::class,'destroy']);
