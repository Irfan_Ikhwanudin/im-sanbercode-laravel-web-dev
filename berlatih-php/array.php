<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Latihan Array</h1>
    <?php
    
    echo "<h3>Soal 1</h3>";

    $kids = [
        "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven <br>"];
    $adult = [
        "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
    ];
    print_r ($kids);
    print_r ($adult);
    
    echo "<h3> Soal 2</h3>";
    $jumlahkids = count($kids);
    echo "Total Trainer: $jumlahkids <br>";
    echo "<ul>";
    echo "<li>$kids[0] </li>";
    echo "<li>$kids[1] </li>";
    echo "<li>$kids[2] </li>";
    echo "<li>$kids[3] </li>";
    echo "<li>$kids[4] </li>";
    echo "</ul>";

    $jumlahadult = count($adult);
    echo "Total Trainer: $jumlahadult <br>";
    echo "<ul>";
    echo "<li>$adult[0] </li>";
    echo "<li>$adult[1] </li>";
    echo "<li>$adult[2] </li>";
    echo "<li>$adult[3] </li>";
    echo "<li>$adult[4] </li>";
    echo "</ul>";


    echo "<h3> Soal 3</h3>";

    $biotrainer = [
        ["nama" => "Will Byers","Age" => "12","Aliases" => "Will the Wise", "Status" =>"Alive" ],
        ["nama" => "Mike Wheeler","Age" => "12","Aliases" => "Dungeon Master", "Status" =>"Alive" ],
        ["nama" => "Jim Hopper","Age" => "43","Aliases" => "Chief Hopper", "Status" =>"Deceased" ],
        ["nama" => "Eleven","Age" => "12","Aliases" => "El", "Status" =>"Alive" ]
    ];

    echo "<pre>";
    print_r($biotrainer);
    echo "</pre>";
        ?>
</body>
</html>