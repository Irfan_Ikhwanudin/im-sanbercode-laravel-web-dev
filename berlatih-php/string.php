<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>

    <?php 

    echo "<h3> Soal no 1</h3>";  
    /*SOAL NO 1
    Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! */
 

    $kalimat1 ="Hello PHP";
    $panjangKalimat1 = strlen($kalimat1);
    $jumlahKataKalimat1 = str_word_count($kalimat1);


    echo "kalimat 1 :$kalimat1 <br>";
    echo "Panjang Kalimat 1= $panjangKalimat1<br>";
    echo "Jumlah Kata Kalimat 1 : $jumlahKataKalimat1 <br>";

    $kalimat1b ="I'm ready for the challenges";
    $panjangKalimat1b = strlen($kalimat1b);
    $jumlahKataKalimat1b = str_word_count($kalimat1b);


    echo "kalimat 1 :$kalimat1b <br>";
    echo "Panjang Kalimat 1= $panjangKalimat1b<br>";
    echo "Jumlah Kata Kalimat 1 : $jumlahKataKalimat1b <br>";

 /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
    echo "<h3> Soal No 2</h3>";
    $kalimat2 = "I love PHP";
    $kata1Kalimat2 = substr($kalimat2, 0, 1);
    $kata2Kalimat2 = substr($kalimat2, 2, 5);
    $kata3Kalimat2 = substr($kalimat2, 6, 8);

   
   
    echo "Kata 1 Kalimat 2 : $kata1Kalimat2<br>";
    echo "Kata 2 Kalimat 2 : $kata2Kalimat2<br>";
    echo "Kata 3 Kalimat 2 : $kata3Kalimat2<br>";

 /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
    echo "<h3> Soal 3</h3>";
    $kalimat3 = " PHP is old but sexy!";


    $gantiStringKalimat3 = str_replace ("sexy", "awesome", $kalimat3);

    echo "kalimat 3 : $kalimat3 <br><br>";

    echo "ubah Kalimat 3 : $gantiStringKalimat3 <br>";
   ?>
</body>
</html>